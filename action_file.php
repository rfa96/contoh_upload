<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 2019-03-17
 * Time: 00:27
 */
    include 'koneksi.php';

    //nentuin action-nya, bisa CREATE, UPDATE, ataupun DELETE
    $action = $_GET['action'];

    //Parameter Form, pakai POST
    $name = $_POST['name'];
    $description = $_POST['description'];

    //---Untuk upload file---
    $nama_file = $_FILES['file']['name'];
    $tmp_file = $_FILES['file']['tmp_name'];
    //Isikan dengan direktori tujuan upload
    $dest_file = 'wp-contents/files/'.$nama_file;
    move_uploaded_file($tmp_file, $dest_file);
    chmod($nama_file, 0777);

    //Cek action
    if($action == 'create')
    {
        //Waktu kapan di-add
        $created_at = date("Y-m-d H:i:s");

        //Insert to DB
        $conn->query("INSERT INTO files(name, description, file, created_at) VALUES ('$name', '$description', '$nama_file', '$created_at')");
    }
    else if($action == 'delete')
    {
        //Ambil ID
        $id = $_GET['id'];

        //Ambil nama file untuk delete file di server. unlink() digunakan untuk menghapus file di server
        $sql_namaFile = $conn->query("SELECT * FROM files WHERE id = $id");
        $fetch_namaFile = $sql_namaFile->fetch_array();
        $nama_ambil  = $fetch_namaFile['file'];
        unlink('wp-contents/files/'.$nama_ambil);

        //Delete data in DB
        $conn->query("DELETE FROM files WHERE id = $id");
    }
    else
    {
        //Ambil ID
        $id = $_GET['id'];

        //Waktu kapan di-update
        $updated_at = date("Y-m-d H:i:s");

        //Cek jika file ingin diganti atau tidak
        if(empty($nama_file))
        {
            //Jika syarat ini terpenuhi, maka tidak ada pengharuhnya dengan file
            $conn->query("UPDATE files SET name = '$name', description = '$description', updated_at = '$updated_at' WHERE id = $id");
        }
        else
        {
            //Jika syarat ini tidak terpenuhi, maka file akan di-update
            $sql_namaFile = $conn->query("SELECT * FROM files WHERE id = $id");
            $fetch_namaFile = $sql_namaFile->fetch_array();
            $nama_ambil  = $fetch_namaFile['file'];
            unlink('wp-contents/files/'.$nama_ambil);
            $conn->query("UPDATE files SET name = '$name', description = '$description', file = '$nama_file', updated_at = '$updated_at' WHERE id = $id");
        }
    }

    header("Location: index.php");
