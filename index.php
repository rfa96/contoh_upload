<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 2019-03-16
 * Time: 23:09
 */
    include 'koneksi.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Index Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container" style="padding-top: 20px">
            <h2>Index Page</h2>
            <hr/>

            <!--- Images --->
            <h5>Images</h5>
            <a class="btn btn-primary" href="form_image.php?crud=create" style="margin-bottom: 10px">Add Image</a>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>File</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $sql_tampil_gambar = $conn->query("SELECT * FROM gambar");
                        $id_gambar_tabel = 1;
                        while($row_gambar = $sql_tampil_gambar->fetch_array())
                        {
                            ?>
                                <tr>
                                    <td><?= $id_gambar_tabel++?></td>
                                    <td><?= $row_gambar['name']?></td>
                                    <td><?= $row_gambar['description']?></td>
                                    <td>
                                        <img src="wp-contents/images/<?= $row_gambar['file']?>" width="200" height="200">
                                    </td>
                                    <td align="center">
                                        <a href="form_image.php?crud=edit&id=<?= $row_gambar['id']?>" class="btn btn-success">EDIT</a>
                                        <a href="action_image.php?action=delete&id=<?= $row_gambar['id']?>" class="btn btn-danger" onclick="return confirm('Yakin menghapus data ini?')">DELETE</a>
                                    </td>
                                </tr>
                            <?php
                        }
                    ?>
                </tbody>
            </table>
            <hr/>

            <!--- File --->
            <h5>File</h5>
            <a class="btn btn-primary" href="form_files.php?crud=create" style="margin-bottom: 10px">Add File</a>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>File</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $sql_tampil_file = $conn->query("SELECT * FROM files");
                        $id_file_tabel = 1;
                        while($row_file = $sql_tampil_file->fetch_array())
                        {
                            ?>
                                <tr>
                                    <td><?= $id_file_tabel++?></td>
                                    <td><?= $row_file['name']?></td>
                                    <td><?= $row_file['description']?></td>
                                    <td>
                                        <a href="wp-contents/files/<?= $row_file['file']?>" class="btn btn-info" target="_blank">Preview File</a>
                                    </td>
                                    <td align="center">
                                        <a href="form_files.php?crud=edit&id=<?= $row_file['id']?>" class="btn btn-success">EDIT</a>
                                        <a href="action_file.php?action=delete&id=<?= $row_file['id']?>" class="btn btn-danger" onclick="return confirm('Yakin menghapus data ini?')">DELETE</a>
                                    </td>
                                </tr>
                            <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </body>
</html>
