<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 2019-03-17
 * Time: 00:25
 */
    include 'koneksi.php';
    $crud = $_GET['crud'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Form Image</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container" style="padding-top: 20px">
            <?php
                if($crud == 'create')
                {
                    ?>
                        <h2>Form Add File</h2>
                        <hr/>
                        <form action="action_file.php?action=create" method="post" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" name="name" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Description</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="description" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Any File (except image)</label>
                                <div class="col-sm-9">
                                    <input type="file" name="file">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    <input type="submit" value="SUBMIT" class="btn btn-success">
                                    <a href="index.php" class="btn btn-danger" onclick="return confirm('Yakin membatalkan proses ini?')">CANCEL</a>
                                </div>
                            </div>
                        </form>
                    <?php
                }
                else
                {
                    $id = $_GET['id'];
                    $sql_edit = $conn->query("SELECT * FROM files WHERE id = $id");
                    $fetch_edit = $sql_edit->fetch_array();
                    ?>
                        <h2>Form Edit File</h2>
                        <hr/>
                        <form action="action_file.php?action=update&id=<?= $fetch_edit['id']?>" method="post" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control" value="<?= $fetch_edit['name']?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Description</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="description" rows="3"><?= $fetch_edit['description']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Image File</label>
                                <div class="col-sm-10">
                                    <input type="file" name="file">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <input type="submit" value="SUBMIT" class="btn btn-success">
                                    <a href="index.php" class="btn btn-danger" onclick="return confirm('Yakin membatalkan proses ini?')">CANCEL</a>
                                </div>
                            </div>
                        </form>
                    <?php
                }
            ?>
        </div>
    </body>
</html>
