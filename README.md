# Uploading Image and Files Best Practice

Requirements: PHP7 and MySQL DB

**Cara _clone_ repo ini:**

1. Buka Terminal (Mac atau Linux) ataupun GIT Terminal (Windows)
2. Arahkan pada direktori htdocs
3. Clone repo ini dengan mengetikkan perintah ```git clone https://rfa96@bitbucket.org/rfa96/contoh_upload.git```
3. Buka ```http://localhost/phpmyadmin```, kemudian import semua data yang ada di file ```contoh_upload.sql```
4. Setelah import DB, buka laman ```http://localhost/contoh_upload``` dan aplikasi siap digunakan.
